$(window).scroll(function() {
   var wH = $(window).height(),
       wS = $(this).scrollTop()+200;
   if (wS > wH){
    $('.content').addClass('visible');
    $('.scrolldown').addClass('hidden');
   }
   else{
   	$('.content').removeClass('visible');
    $('.scrolldown').removeClass('hidden');
   }
});

$(document).ready(function() {

  $('.intro-ui').mouseover(function(e) {
        $('.header-1,.header-3,.intro-montreal').addClass("fade");
        $('.intro-ui-gif').addClass("visible");  
      });
  $('.intro-ui').mouseleave(function(e) {
        $('.header-1,.header-3,.intro-montreal').removeClass("fade");
        $('.intro-ui-gif').removeClass("visible");   
      });
  $('.intro-montreal').mouseenter(function(e) {
        $('.header-1,.header-3,.intro-ui').addClass("fade");
        $('.intro-montreal-gif').addClass("visible");  
      });
  $('.intro-montreal').mouseleave(function(e) {
        $('.header-1,.header-3,.intro-ui').removeClass("fade");
        $('.intro-montreal-gif').removeClass("visible");   
      });

  $('.title-traverse').hover(function(e) {
    $('.gradient').toggleClass("gradient-traverse");
  });  
  $('.title-beavertails').hover(function(e) {
    $('.gradient').toggleClass("gradient-beavertails");
  });
  $('.title-saintstreet').hover(function(e) {
    $('.gradient').toggleClass("gradient-saintstreet");
  });
  $('.title-nhl').hover(function(e) {
    $('.gradient').toggleClass("gradient-nhl");
  });
  $('.title-stromung').hover(function(e) {
    $('.gradient').toggleClass("gradient-stromung");
  });
});